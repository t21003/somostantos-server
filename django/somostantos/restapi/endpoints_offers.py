import json
import smtplib, ssl

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .models import Tevent, Tuser, Toffer

# POST /version/1/events/{id}/offers
@csrf_exempt
def publicarOfertaEvento(request, event_id):
    if request.method == "POST":
        # 404: La petición ha fallado porque no existe un evento con el ID especificado
        try:
            event = Tevent.objects.get(id=event_id)
        except Tevent.DoesNotExist:
            return JsonResponse(status=404, data={'message':'No existe un evento con el ID especificado'})

        # 401: La petición ha fallado porque no se ha enviado el token de autenticación en las cabeceras
        try:
            sessionToken = request.headers['token']
        except KeyError:
            return JsonResponse(status=401, data={'message':'No se ha enviado el token de autenticación en las cabeceras'})
        # 403: La petición ha fallado porque el token de sesión no es válido
        try:
            business_user_id = Tuser.objects.get(active_session_token = sessionToken)
        except Tuser.DoesNotExist:
            return JsonResponse(status=403, data={'message':'El token de sesión no es válido'})
                
        try:
            # 201: La oferta se ha creado con éxito
            json_body = json.loads(request.body)
            price = json_body['total_price']
            info = json_body['extra_info']
            q = Toffer(bussiness_user = business_user_id, event = event, total_price = price, extra_info = info, is_accepted = False)
            q.save()
            return JsonResponse(status=201,data={'message':'La oferta se ha creado con éxito'})
        except KeyError:
            return JsonResponse(status=400,data={'message':'No se han enviado valores en el body'})
    else:
        return JsonResponse(status=405,data={'message':'No se ha utilizado el método de petición POST'})

# POST /version/1/events/{id}/offers/{offer_id}
@csrf_exempt
def aceptarOfertaEvent(request, event_id, offer_id):
    if request.method == "POST":
        # 404: La petición ha fallado porque no existe un evento con el ID especificado o una oferta con el ID especificado.
        try:
            offer = Toffer.objects.get(id=offer_id, event=event_id)
        except Toffer.DoesNotExist:
            return JsonResponse(status=404, data={'message':'No existe un evento con el ID especificado o una oferta con el ID especificado.'})

        # 401: La petición ha fallado porque no se ha enviado el token de autenticación en las cabeceras.
        try:
            sessionToken = request.headers['token']
            # 403: La petición ha fallado porque el token de sesión no es válido
            try:
                Tuser.objects.get(id=offer.bussiness_user.id, active_session_token=sessionToken)
            except Tuser.DoesNotExist:
                return JsonResponse(status=403, data={'message':'El token de sesión no es válido.'})
        except KeyError:
            return JsonResponse(status=401, data={'message':'No se ha enviado el token de autenticación en las cabeceras.'})

        # 201: La oferta se ha aceptado con éxito. El local recibirá un e-mail de confirmación con el nombre y e-mail del cliente, 
        # y el atributo 'is_accepted' de la oferta se grabará a true en la base de datos.
        if offer.is_accepted == False:
            password = "Password"
            receiver = offer.bussiness_user.email
            sendEmail(receiver, event_id, password)
            offer.is_accepted = True
            offer.save()
            return JsonResponse(status=201,data={'message':'La oferta se ha aceptado con éxito.'})
        else:
            return JsonResponse(status=400,data={'message':'La oferta ya está confirmada'})
    else:
        return JsonResponse(status=405,data={'message':'No se ha utilizado el método de petición POST'})

def sendEmail(receiver, event_id, password):
    sender = "t2notifications_no_reply@fpcoruna.afundacion.org"
    message = """Subject: Correo de Confirmacion

    Se ha confirmado la Oferta para el Evento con el ID: """ + str(event_id)

    port = 587
    server = "smtp.gmail.com"

    context = ssl.create_default_context()
    with smtplib.SMTP(server, port) as server:
        server.starttls(context=context)
        server.login(sender,password)
        server.sendmail(sender,receiver,message)
