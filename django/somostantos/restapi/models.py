# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Tevent(models.Model):
    datetime = models.DateTimeField()
    number_attendants = models.IntegerField()
    latitude = models.DecimalField(max_digits=8, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    author = models.ForeignKey('Tuser', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'tEvent'


class Toffer(models.Model):
    bussiness_user = models.ForeignKey('Tuser', models.DO_NOTHING)
    event = models.ForeignKey(Tevent, models.DO_NOTHING)
    total_price = models.IntegerField()
    extra_info = models.CharField(max_length=500, blank=True, null=True)
    is_accepted = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'tOffer'


class Tuser(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=100)
    email = models.CharField(unique=True, max_length=200)
    encrypted_password = models.CharField(max_length=100)
    business_name = models.CharField(max_length=100, blank=True, null=True)
    business_latitude = models.DecimalField(max_digits=8, decimal_places=6, blank=True, null=True)
    business_longitude = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    active_session_token = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tUser'
