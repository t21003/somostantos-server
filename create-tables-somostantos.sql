CREATE DATABASE IF NOT EXISTS somostantosdb;

USE somostantosdb;

CREATE TABLE tUser (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  surname VARCHAR(100) NOT NULL,
  email VARCHAR(200) NOT NULL UNIQUE,
  encrypted_password VARCHAR(100) NOT NULL,
  business_name VARCHAR(100),
  business_latitude DECIMAL(8,6),
  business_longitude DECIMAL(9,6),
  active_session_token CHAR(20)
);

CREATE TABLE tEvent (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  datetime DATETIME NOT NULL,
  number_attendants INTEGER NOT NULL,
  latitude DECIMAL(8,6) NOT NULL,
  longitude DECIMAL(9,6) NOT NULL,
  author_id INTEGER NOT NULL,
  
  FOREIGN KEY (author_id) REFERENCES tUser(id)
);

CREATE TABLE tOffer (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  bussiness_user_id INTEGER NOT NULL,
  event_id INTEGER NOT NULL,
  total_price INTEGER NOT NULL,
  extra_info VARCHAR(500),
  is_accepted BOOLEAN NOT NULL DEFAULT FALSE,
  
  FOREIGN KEY (bussiness_user_id) REFERENCES tUser(id),
  FOREIGN KEY (event_id) REFERENCES tEvent(id)
);
