<?php
ini_set('display_errors', 'On');
require __DIR__ . '/../php_util/db_connection.php';

$mysqli = get_db_connection_or_die();

$name = $_POST['name'];
$surname = $_POST['surname'];
$email = $_POST['email'];
$pass = $_POST['pass'];
$pass2 = $_POST['pass2'];

$business_name = $_POST['business_name'];
$business_latitude = $_POST['business_latitude'];
$business_longitude = $_POST['business_longitude'];

if (empty($name) or empty($email) or empty($surname) or empty($pass)) {
  die("Faltan datos importantes para el registro");
}
if ($pass !== $pass2) {
  die("Las contraseñas no son iguales");
}
if ($business_name == '') {
  $business_name = NULL;
}


try {
  $sql = "INSERT INTO tUser (name, surname, email, encrypted_password, business_name, business_latitude, business_longitude) VALUES (?, ?, ?, ?, ?, ?, ?)";
  $stmt = $mysqli->prepare($sql);
  $stmt->bind_param("sssssdd", $name, $surname, $email, password_hash($pass, PASSWORD_BCRYPT), $business_name, $business_latitude,  $business_longitude);
  $stmt->execute();
  if (!empty($mysqli->error)) {
    header("Location: register.php?register_failed_email=True");
    exit();
  }
  $stmt->close();
} catch (Exception $e) {
  error_log($e);
  header("Location: register.php?register_failed_unknown=True");
  exit();
}

header("Location: register.php?success=True");
