<?php
ini_set('display_errors', 'On');
require __DIR__ . '/../php_util/db_connection.php';
session_start();
$mysqli = get_db_connection_or_die();
$user_id = $_SESSION['user_id'];

/**
 * Función para calcular la distancia entre dos punto
 * @returns distancia en km
 */
function distance($lat1, $lon1, $lat2, $lon2)
{
	$pi80 = M_PI / 180;
	$lat1 *= $pi80;
	$lon1 *= $pi80;
	$lat2 *= $pi80;
	$lon2 *= $pi80;
	$r = 6372.797; // mean radius of Earth in km 
	$dlat = $lat2 - $lat1;
	$dlon = $lon2 - $lon1;
	$a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
	$c = 2 * atan2(sqrt($a), sqrt(1 - $a));
	$km = $r * $c;
	return $km;
}
?>

<!DOCTYPE html>

<head>
	<meta charset="UTF8">
	<title>ST | Inicio</title>
	<!--link estilos css-->
	<link rel="stylesheet" type="text/css" href="/static/style.css" />
</head>

<body>
	<!--Imagen fondo-->
	<div class="fondo">
		<img class="img_fondo" src="/static/fondo.jpg" alt="Fondo" width="100%" height="100%">
	</div>
	<!--Boton logout-->

	<!--Logo-->
	<div class="logo1">
		<img src="/static/logo.png" alt="logo" height="100px" width="100px">
	</div>
	<!--Contenido PHP-->
	<div class="container3">
		<div class="boton_logout">
			<button onclick="window.location.href='/logout.php'">Logout</button>
		</div>
		<?php
		#Comprobamos que la sesión no está vacía
		if (empty($user_id)) {
			header('Location: error.php?mensaje=Error');
		} else {
			#Instanciamos una variable donde cogemos todos los datos del usuario que tenga como id el comprobado anteriormente
			$business_name = 'SELECT * FROM tUser WHERE id=' . $user_id;
			$result = mysqli_query($mysqli, $business_name) or die('Query Error');
			#Almacenamos en la variable result la query
			$row1 = mysqli_fetch_array($result);
			#Comprobamos que la casilla de la columna business_name es nula para ese usuario
			if (is_null($row1['business_name'])) {
				echo '<br>';
				echo '<br>';
				echo '<br>';
				echo '<br>';
				echo '<h1>TUS EVENTOS</h1>';
				#Creamos una variable que nos almacene toda la información de los eventos de ese usuario
				$query = 'SELECT * FROM tEvent WHERE author_id =' . $user_id;
				$result1 = mysqli_query($mysqli, $query) or die('Query Error');
				echo '<table class="default">';
				echo '<tr>';
				echo '<th>FECHA/HORA</th>';
				echo '<th>NUMERO COMENSALES</th>';
				echo '<th>ENLACE EVENTOS</th>';
				echo '</tr>';
				#Recorremos $result1, almacenando los datos en un array
				while ($row = mysqli_fetch_array($result1)) {
					echo '<tr>';
					#Mostramos los datos que queremos
					echo '<td>' . $row['datetime'] . '</td>';
					echo '<td>' . $row['number_attendants'] . '</td>';
					echo '<td><a href="event.php?id=' . $row['id'] . '">Enlace evento Nº ' . $row['id'] . '</td>';
					echo '</tr>';
				}

				echo '</table>';
				#Cerramos la conexión
				mysqli_close($mysqli);


		?>
				<div class="superior">
					<p>¿Quieres añadir un nuevo evento?</p>
					<div class="botones1">
						<button class="evento" onclick="window.location.href='/create_event.php'">Crear Evento</button>
					</div>
				</div>

		<?php
				// Comprobar que el usuario es un dueño
			}
			if (!is_null($row1['business_name'])) {
				echo "<h1>EVENTOS CERCANOS</h1>";
				$query = 'SELECT * FROM tUser WHERE id =' . $user_id;
				$result = mysqli_query($mysqli, $query) or die('Query Error');
				$row = mysqli_fetch_array($result);
				$position_bussiness = [$row['business_latitude'], $row['business_longitude']];

				//Recoger todos los eventos
				$stmt = $mysqli->prepare("SELECT * FROM tEvent");
				$stmt->execute();
				$result = $stmt->get_result();
				// Comprobar si los eventos están cerca del negocio
				while ($row = mysqli_fetch_array($result)) {
					$distancia = distance($row['latitude'], $row['longitude'], $position_bussiness[0], $position_bussiness[1]);
					if ($distancia < 1) {
						$query_name_user = 'SELECT name FROM tUser WHERE id = ' . $row['author_id'];
						$result_name_user = mysqli_query($mysqli, $query_name_user) or die('Query Error');
						$row_user = mysqli_fetch_array($result_name_user);
						echo '<hr>';
						echo '<td><h4>Evento de ' . $row_user['name'] . '</h4></td> <br>';
						echo '<td> Fecha: ' . $row['datetime'] . '</td> <br>';
						echo '<td> Número de integrantes: ' . $row['number_attendants'] . '</td> <br>';
						echo '<td>Distancia: ' . round($distancia * 1000) . ' m</td><br>';
						echo '<td><a href="offer.php?event_id=' . $row['id'] . '&author_id=' . $row['author_id'] . '">Hacer oferta</a></td>';
						echo '</tr>';
					}
				}
			} else {
				die('Pagina no implementada');
			}
		}

		?>
	</div>
</body>

</html>