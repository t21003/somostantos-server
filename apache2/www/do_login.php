<?php
    ini_set('display_errors', 'On'); // Something useful!
    require __DIR__ . '/../php_util/db_connection.php';

$mysql = get_db_connection_or_die();
$fmail = $_POST['fmail'];
$fpass = $_POST['fpass'];

if (empty($fmail) or empty($fpass)) {
    die("Faltan datos");

}
$query = "SELECT id, encrypted_password FROM tUser WHERE email = '".$fmail."'";
$result = mysqli_query($mysql, $query) or die(header('login.php?login_failed_unknown=True'));
if (mysqli_num_rows($result) > 0) {
    $only_row = mysqli_fetch_array($result);
    if (password_verify($fpass, $only_row[1])) {
        session_start();
        $_SESSION['user_id'] = $only_row[0];
        header('Location: main.php');
    } else {
        header('Location: login.php?login_failed_password=True');}
} else{
    header('Location: login.php?login_failed_email=True');}
?>