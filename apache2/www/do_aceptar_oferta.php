<?php
    ini_set('display_errors', 'On'); // Something useful!
    require __DIR__ . '/../php_util/db_connection.php';

    $mysql = get_db_connection_or_die();
    $id = $_POST['aceptar'];

    try {
        $stmt = $mysql->prepare('UPDATE tOffer SET is_accepted = True WHERE id = ?');
        $stmt->bind_param("i", $id);
        $stmt->execute();
        header('Location: main.php');

    }catch(Exception $e){
        header('Location: main.php?error=True');
    }

    
   



?>