<?php
ini_set('display_errors', 'On');
require __DIR__ . '/../php_util/db_connection.php';

session_start();
$mysqli = get_db_connection_or_die();

$author_id = $_GET['author_id'];

//  Si estas variables estan vacías redirigirá a sus respectivos archivos .
if (empty($_SESSION['user_id'])) {
    header("Location: error.php?mensaje=El usuario no ha iniciado la sesión");
}elseif(empty($_GET['event_id'])){
    header('Location: main.php');
}elseif(empty($_GET['author_id'])){
    header('Location: main.php');
}


//consulta para mostrar el nombre del que ha creado el evento
$query = 'SELECT name FROM tUser WHERE id ='.$author_id;
$result = mysqli_query($mysqli, $query) or die('Query Error');
$row = mysqli_fetch_array($result);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="./static/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
    <div class="logo"><img src="./static/logo.png" alt=""></div>
    <div class="fondo"><img src="./static/imagen2.jpg" alt="" width="100%"></div>

    <div class="container2">
        
    <?php
    echo '<h1> Evento de '.$row['name']. '</h1>';
    ?>
    <?php
      // recoger la variable $_GET['failed'] para mostrar el error
        if (isset($_GET['failed'])) {
            if($_GET['failed'] == 'True'){
                echo ('<p class = "alerta" >La inserción ha fallado. Vuelve a intentarlo</p>');
            }
        }
    ?>
        <form id="formulario" method="post" action="./do_offer.php">
            <div class="row">
                <div class="col-25">
                    <label for=" ofertaDescripcion">Introduce tu propuesta</label>
                </div>
                <div class="col-75">
                    <textarea  name="ofertaDescripcion" id="ofertaDescripcion"  maxlength="500" rows="4" cols="3" style="resize:none;width:100%" required></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-25">
                    <label for="ofertaPrecio">Introduce una oferta</label>
                </div>
                <div class="col-75">
                    <input type="number" name="ofertaPrecio" id="ofertaPrecio"  min="1" step="1" required>
                </div>
            </div>

            <input type='hidden' name='evento_id' id="evento_id" value="<?php echo $_GET['event_id'];?>" />
            <div class="row botones">
                <input class="register" type="submit" value="Enviar">
                <!-- <button type="submit" value="Ejecutar">Ejecutar</button> -->
            </div>
        </form>
    </div>
</body>

</html>

