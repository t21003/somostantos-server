<?php
  //  ini_set('display_errors', 'On'); // Something useful!
 // conexión a la bbdd
    require __DIR__ . '/../php_util/db_connection.php';
    session_start();
     $mysqli = get_db_connection_or_die();
    //recoge el dato del id de usuario de la sesión, se fuerza el valor a 1
    $user_id = $_SESSION['user_id'];
    // recogida de datos de $_POST   
	$nombre = $_POST['nombre'];
    $fecha = $_POST['fecha'];
    $numComensales = $_POST['numComensales'];
    $latitud = 0;
    $longitud = 0;
    $latitud = $_POST['lat'];
    $longitud = $_POST['lon'];

     // comprobación de dato fecha, no debe ser menor que la fecha actual
    $fechanow = new DateTime('NOW');
    $fechanow = $fechanow->format('Y-m-d H:i:s');
   if ($fecha < $fechanow){
            header("Location: create_event.php?failed_date=True");
          exit();
    }
    //inserción de los datos en la bbdd
    try{
        $query = "INSERT INTO tEvent(datetime, number_attendants, latitude, longitude, author_id)
                    VALUES ('".$fecha."',".$numComensales.",".$latitud.",".$longitud.",".$user_id.")";

    // comprobación de que la inserción ha sido correcta, de otro modo devuelve a la pantalla anterior    
    if (!$mysqli->query($query)) {
        header("Location: create_event.php?failed=True");
        exit();
    }
        echo mysqli_insert_id($mysqli);
        mysqli_close($mysqli);
        // si todo ha ido bien, devuelve a la página main.php
        header("Location: main.php");
    }catch(Exception $e){
        error_log($e);
        //si falla vuelve a create_event.php
          header("Location: create_event.php?failed=True");
    }    
    ?> 
