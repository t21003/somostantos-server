<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Error</title>
    <link rel="stylesheet" type="text/css" href="/static/style.css"/>
</head>
<body>
    <!-- muestra las imágenes de la pantalla en la página de error-->
    <div class="logo"><img src="/static/logo.png" alt=""></div>
    <div class="fondo"><img src="/static/imagen2.jpg" alt="" width="100%"></div>
    
    <div class="container2">
        <?php
            // muestra el error enviado en la variable $_GET
            echo "<h2 align = 'center'>".$_GET['mensaje']."</h2>";
        ?>
        <!--botón para volver al login-->
        <div class="botones">
            <button onclick="window.location.href='/login.php'">Login</button>
        </div>
    </div>
   
</body>
</html>
