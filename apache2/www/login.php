<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="/static/style.css">

</head>
<body>
    <div class="logo"><img src="/static/logo.png" alt=""></div>
    <div class="fondo"><img src="/static/imagen2.jpg" alt="" width="100%"></div>
    <div class="container2">
  <form method="post" action="/do_login.php">
    <div class="row">
      <div class="col-25">
        <?php
          $unknown_fail = $_GET['login_failed_unknown'];
          if($unknown_fail == TRUE) {
            echo ('<p class="alerta">*HA OCURRIDO UN ERROR INESPERADO</p>');
          }
        ?>
        <label for="fmail">E-mail</label>
        <?php
          $email_fail = $_GET['login_failed_email'];
          if($email_fail == TRUE) {
            echo ('<p class="alerta">*El e-mail es incorrecto</p>');
          }
        ?>
      </div>
      <div class="col-75">
        <input type="text" id="fmail" name="fmail" placeholder="Tu e-mail...">
      </div>
    </div>
    <div class="row">
      <div class="col-25">
        <label for="fpass">Contraseña</label>
        <?php
          $pass_fail = $_GET['login_failed_password'];
          if($pass_fail == TRUE) {
            echo ('<p class="alerta">*La contraseña es incorrecta</p>');
          }
        ?>
      </div>
      <div class="col-75">
        <input type="password" id="fpass" name="fpass" placeholder="Tu contraseña...">
      </div>
    </div>
    <div class="row botones">
      <input type="submit" value="Iniciar sesión"/>
      
      <button type="button" onclick="window.location.href='/register.php'">Registarse</button>

    </div>
    </div>
  </form>
</div>
</div>
</body>
</html>